<br>
<br>
<br>
<br>
<br>
<br>
</br><!DOCTYPE html>

<html>

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title>LLH | Online Payment</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../assets-design/frontend/sspkm/img/logo.png">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="../../maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets-design/frontend/sspkm/css/styleec74.css?v=0.07" />
    <link rel="stylesheet" type="text/css" href="../assets-design/frontend/sspkm/css/style-responsivea139.html?v=0.01" />
    
    <link rel="stylesheet" href="../../cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css">
    <!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">-->


    <link rel="stylesheet" type="text/css" href="../../unpkg.com/aos%402.3.0/dist/aos.css" />
    
    <script src="../assets-design/frontend/sspkm/js/jquery-3.4.1.js"></script>
    <script src="../../stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
    <style>
        .footer-column-content{
            width:max-content;
            margin:auto;
        }
        @media screen and (max-width:1252px){
            header .logo img{
                margin-left:0;
            }
        }
        @media screen and (max-width:992px){
            header .logo h3{
                font-size:18px;
            }
            .sticky .logo h3 {
                font-size: 16px;
            }
            header .logo img{
                width:80px;
            }
            .sticky .logo img{
                width:75px;
            }
            header .pc-nav{
                margin:6px 0;
                margin-top:15px;
            }
            .sticky .pc-nav{
                margin-top:10px;
            }
            header .pc-nav li a{
                padding: 1px 0;
                margin: 0 6px;
                font-size: 15px;
            }
        }
        @media screen and (max-width:768px){
            .footer-column-content{
                margin-left:30px;
            }
            .social{
                text-align:center;
            }
        }
        .scroll-top {
            width: 50px;
            height: 50px;
            position: fixed;
            bottom: 25px;
            right: 20px;
            display: none;
            opacity: 0.6;
        }
        .scroll-top i {
            display: inline-block;
            color: #FFFFFF;
        }
    </style>
</head>
<body>
    
<section id="facilities">
	<div class="container">
		<div class="row">
		<div class="col-md-12">
		    <div style="border:1px solid black;margin-top:10px;padding:20px">
		        <h3>NOTICE:</h3>
               <p>જો તમે UPI દ્વારા ફી ભરો છો.</p>
                <p>તમે જે UPI એપ્લિકેશન દ્વારા ફી ભરતા હો તે મોબાઇલના બદલે બીજા મોબાઇલ અથવા લેપટોપ/ કમ્પ્યુટરમાં વેબસાઇટ ખોલો અને ફી ચૂકવો</p>
                <p>If you pay a fee through UPI.</p>
                <p>open the website in another mobile or laptop / computer instead of the mobile through which you pay the fee through UPI application and pay the fee</p>
                </div>
           </div>
				 <div class="col-lg-12">
           <h3>Regular Students Only Pay Fees Online</h3>
           
            <div class="form">
              
                                           <form  action="https://www.sspkm.org/student/VarifyStudent/varify" method="post" role="form" enctype="multipart/form-data" class="contactForm">
               <input type="hidden" name="csrf_test_name" value="da99bbfe2517c752f3f68489657c97b3">        
                 
                <fieldset>
                <div class="row secborder">
                   <div class="form-group col-lg-3">
                      <label><small>Registration No <span>*</span></small></label>
                      <input  type="text" name="form_no" class="form-control" placeholder=""  required />
                      <div class="validation"></div>
                    </div>
                    <div class="form-group col-lg-3">
                      <label><small>Mobile No<span>*</span></small></label>
                      <input  type="text" name="mobile_no"   class="form-control" placeholder=""  required />
                      <div class="validation"></div>
                    </div>
                    <div class="form-group col-lg-3">
                        <label></label>
                        <div class="text-center">
                      <button type="submit" id="sbmt_btn" class="btn btn-primary" title="Send Message">Submit</button>
                  </div>
                    </div>
                  
                 
                </div>
                   
                   
                  
                
                </fieldset>
              </form>
               
            </div>
            </br>
            </br>
            </br>
            </br>
          </div>
		
		</div>
	</div>
</section>

    
<button class="btn btn-primary scroll-top" data-scroll="up" type="button">
<i class="fa fa-chevron-up"></i>
</button>
<script src="../assets-design/frontend/sspkm/js/main9093.js?v=0.02"></script>
<script src="../../unpkg.com/aos%402.3.0/dist/aos.js"></script>
<script>
    AOS.init({
  duration: 1200,
})
</script>
<script>
    window.onscroll = function() {myFunction()};
    
    var header = document.getElementById("header");
    var sticky = header.offsetTop;
    
    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
        $(".sticky-hide").fadeOut();
      } else {
        header.classList.remove("sticky");
        $(".sticky-hide").fadeIn();
      }
    }
</script>
</body>

<!-- Mirrored from www.sspkm.org/home/pay_fees by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 26 Aug 2021 09:01:43 GMT -->
</html>