<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<body>
 
    <style>
    #details{
        padding:40px 0;
    }
    #details h1,#details h4{
        font-weight:900;
    }
    #details i{
        font-size:60px;
        color:#222B34;
    }
    #details p{
        font-size:16x;
    }
    #maps{
        padding:40px 0;
    }
    #maps h3{
        margin-bottom:30px;
    }
    #maps input{
        margin-bottom:30px;
    }
    @media screen and (max-width:768px){
        #banner{
            height: 90px;
        }
        #banner h1{
            margin-top: 30px;
            font-size: 25px;
        }
        #banner img{
            height:90px;
        }
    }
</style>
<section id="banner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1>Contact <span>Us</span></h1>
            </div>
        </div>
    </div>
</section>
<section id="details">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-4 col-sm-4">
                <i class="fa fa-map-marker"></i>
                <h2>Address</h2>
                <h4>LLH Hostel</h4>
                <p>Live LIke home , Near new IIM rd, Vastrapur, Ahmedabad, Gujarat-380009</p>
            </div>
            <div class="col-md-4 col-sm-4">
                <i class="fa fa-phone"></i>
                <h2>Phone No.</h2>
                <p>
                    +91 079 26307099<br>
                    +91 63520 96322<br>
                   </p>
            </div>
            <div class="col-md-4 col-sm-4">
                <i class="fa fa-envelope"></i>
                <h2>E-Mail Id</h2>
                <p>llhhostel@gmail.com</p>
            </div>
            <!-- <div class="col-md-3 col-sm-3">
                <i class="fa fa-map-marker"></i>
                <h2>Address</h2>
                <h4>Girl’s Hostel</h4>
                <p>Bhagirath Rd, Kalupura, Memnagar, Ahmedabad, Gujarat 380052</p>
            </div> -->
        </div>
    </div>
</section>
<section id="maps">
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-md-8 col-sm-8">
                <h3>LLH Hostel Map</h3>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.718594712607!2d72.52884751544549!3d23.03410238494718!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e84c81749330b%3A0x2f997bf6eedcba4!2sLJ%20College%20of%20Computer%20Applications!5e0!3m2!1sen!2sin!4v1642938072622!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
            <div class="col-md-4 col-sm-4">
                <h3>Inquiry Now</h3>
                <form action="#" method="POST">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="Name" />
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email" />
                    </div>
                    <div class="form-group">
                        <input type="tel" name="mobile" class="form-control" placeholder="Mobile No." />
                    </div>
                    <div class="form-group">
                        <textarea name="message" class="form-control" placeholder="Comments"></textarea>
                    </div>
                    <input type="submit" value="Submit" class="btn btn-primary" />
                </form>
            </div>
            <!-- <div class="col-md-4 col-sm-4">
                <h3>Girl's Hostel Map</h3>
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14684.968852837195!2d72.543521!3d23.05158!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6de18291b7547e62!2sSaurashtra%20Patel%20Kanya%20Chhattralaya!5e0!3m2!1sen!2sin!4v1598109388053!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div> -->
        </div>
    </div>
</section>    
