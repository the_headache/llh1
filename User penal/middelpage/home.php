<!-- start -->
<section>
  <div class="swiper mySwiper">
      <div class="swiper-wrapper">
        <!-- <div class="swiper-slide">
            <img src="..\User penal\assets-design\frontend\sspkm\img\slider\hostel1.png" style="width:100%">
        </div> -->
        <div class="swiper-slide">
            <img src="..\User penal\assets-design\frontend\sspkm\img\slider\slider1.png" style="width:100%">
        </div>
        <div class="swiper-slide">
            <img src="..\User penal\assets-design\frontend\sspkm\img\slider\home.jpg" style="width:100%">
        </div>
      </div>
      <div class="swiper-pagination"></div>
  </div>
</section>
<!-- end -->
<section id="about-us">
   <div class="container">
       <div class="row">
           <div class="col-md-7">
                <h3>About <span class="bold900">Us</span></h3>
                <hr class="title-underline">
               <div class="content">
                    <p>
                       Live Like Home (LLH) is accomplishing wonderful task by providing opportunity to the students of through the medium of accommodation in huge city like Ahmadabad and encouraging them to pursue higher and better education.
                   </p>
                      <p>
                       It aims at providing comfortable, hygienic accommodations with assured quality standards at affordable rates. All in all, the hostel life in LLH is one of the most enviable among the student community, providing students with conducive atmosphere for learning and personal development.
                       </p>
                       <a href="index.php?page=about"><button type="button">Read More</button></a>
               </div>
           </div>
           <div class="col-md-5 news-col">
              <img src="upload/banner.png">
             </div>
       </div>
   </div>
</section>
<section id="facilities">
   <div class="container">
       <div class="row">
           <div class="col-md-12 text-center">
               <h2>Hostel <span class="bold900">Facilities</span></h2>
               <hr class="title-underline"> 
           </div>
       </div>
       <div class="row text-center">
           <div class="col-md-3 col-sm-3">
               <a href="index.php?page=h1">
                   <img data-aos="zoom-in-up" src="assets-design/frontend/sspkm/img/facilities/room.png" />
                   <h3>Room</h3>
                   <p>Boys Hostel consists of two units. The Unit-1 is of ten floors which…</p>
               </a>
           </div>
           <div class="col-md-3 col-sm-3">
               <a href="index.php?page=h3">
                   <img data-aos="zoom-in-up" src="assets-design/frontend/sspkm/img/facilities/library.png" />
                   <h3>Library</h3>
                   <p>A huge 24 X 7 hours open library is provided in both the hostels…</p>
               </a>
           </div>
           <div class="col-md-3 col-sm-3">
               <a href="index.php?page=h2">
                   <img data-aos="zoom-in-up" src="assets-design/frontend/sspkm/img/facilities/dining.png" />
                   <h3>Dining</h3>
                   <p>Healthy and nourishing food is also provided in the dining halls…</p>
               </a>
           </div>
           <div class="col-md-3 col-sm-3">
               <a href="index.php?page=h4">
                   <img data-aos="zoom-in-up" src="assets-design/frontend/sspkm/img/facilities/gym.png" />
                   <h3>Gym</h3>
                   <p>Boys can be in good shape and maintain their strength by the…</p>
               </a>
           </div>
       </div>
       <div class="row">
           <div class="col-md-12 text-center">
               <a href="index.php?page=hostel_facilities"><button class="theme-btn" type="button">View All Facilities</button></a>
           </div>
       </div>
   </div>
</section>
<section id="events">
   <div class="container-fluid">
       <div class="row">
           <div class="col-md-12 text-center">
               <h2>Hostel Events and <span class="bold900">Functions Celebration</span></h2>
               <hr class="title-underline">
           </div>
       </div>
       <div class="row">
            <div class="col-md-12">
                <div class="row grid hes-gallery" data-wrap="false" data-img-count="false">
                                        <div class="col-md-3 col-sm-3 effect-ming">
                    		<img data-aos="flip-left" src="https://www.sspkm.org/documents/hostel/gallery/1602329903_photo_documents.JPG" style="height: 200px;width:98%" data-subtext="6th Sports Day"   />
                    		<figcaption>
                    			<p style="background-color: white;padding: 10px;font-weight:bold;font-size:15px;font-size: 18px;position: absolute;bottom: 15px;left: 0;right: 0;">6th Sports Day</p>
                    		</figcaption>			
                    </div>
                                        <div class="col-md-3 col-sm-3 effect-ming">
                    		<img data-aos="flip-left" src="https://www.sspkm.org/documents/hostel/gallery/1602425287_photo_documents.JPG" style="height: 200px;width:98%" data-subtext="6th Sports Day"   />
                    		<figcaption>
                    			<p style="background-color: white;padding: 10px;font-weight:bold;font-size:15px;font-size: 18px;position: absolute;bottom: 15px;left: 0;right: 0;">6th Sports Day</p>
                    		</figcaption>			
                    </div>
                                        <div class="col-md-3 col-sm-3 effect-ming">
                    		<img data-aos="flip-left" src="https://www.sspkm.org/documents/hostel/gallery/1602425209_photo_documents.JPG" style="height: 200px;width:98%" data-subtext="6th Sports Day"   />
                    		<figcaption>
                    			<p style="background-color: white;padding: 10px;font-weight:bold;font-size:15px;font-size: 18px;position: absolute;bottom: 15px;left: 0;right: 0;">6th Sports Day</p>
                    		</figcaption>			
                    </div>
                                        <div class="col-md-3 col-sm-3 effect-ming">
                    		<img data-aos="flip-left" src="https://www.sspkm.org/documents/hostel/gallery/1602330240_photo_documents.JPG" style="height: 200px;width:98%" data-subtext="6th Sports Day"   />
                    		<figcaption>
                    			<p style="background-color: white;padding: 10px;font-weight:bold;font-size:15px;font-size: 18px;position: absolute;bottom: 15px;left: 0;right: 0;">6th Sports Day</p>
                    		</figcaption>			
                    </div>
                                        <div class="col-md-3 col-sm-3 effect-ming">
                    		<img data-aos="flip-left" src="https://www.sspkm.org/documents/hostel/gallery/1602328443_photo_documents.JPG" style="height: 200px;width:98%" data-subtext="6th Sports Day"   />
                    		<figcaption>
                    			<p style="background-color: white;padding: 10px;font-weight:bold;font-size:15px;font-size: 18px;position: absolute;bottom: 15px;left: 0;right: 0;">6th Sports Day</p>
                    		</figcaption>			
                    </div>
                                        <div class="col-md-3 col-sm-3 effect-ming">
                    		<img data-aos="flip-left" src="https://www.sspkm.org/documents/hostel/gallery/1602424670_photo_documents.JPG" style="height: 200px;width:98%" data-subtext="6th Sports Day"   />
                    		<figcaption>
                    			<p style="background-color: white;padding: 10px;font-weight:bold;font-size:15px;font-size: 18px;position: absolute;bottom: 15px;left: 0;right: 0;">6th Sports Day</p>
                    		</figcaption>			
                    </div>
                                        <div class="col-md-3 col-sm-3 effect-ming">
                    		<img data-aos="flip-left" src="https://www.sspkm.org/documents/hostel/gallery/1602329947_photo_documents.JPG" style="height: 200px;width:98%" data-subtext="6th Sports Day"   />
                    		<figcaption>
                    			<p style="background-color: white;padding: 10px;font-weight:bold;font-size:15px;font-size: 18px;position: absolute;bottom: 15px;left: 0;right: 0;">6th Sports Day</p>
                    		</figcaption>			
                    </div>
                                        <div class="col-md-3 col-sm-3 effect-ming">
                    		<img data-aos="flip-left" src="https://www.sspkm.org/documents/hostel/gallery/1602328965_photo_documents.JPG" style="height: 200px;width:98%" data-subtext="6th Sports Day"   />
                    		<figcaption>
                    			<p style="background-color: white;padding: 10px;font-weight:bold;font-size:15px;font-size: 18px;position: absolute;bottom: 15px;left: 0;right: 0;">6th Sports Day</p>
                    		</figcaption>			
                    </div>
                                        <div class="col-md-3 col-sm-3 effect-ming">
                    		<img data-aos="flip-left" src="https://www.sspkm.org/documents/hostel/gallery/1602828136_photo_documents.jpg" style="height: 200px;width:98%" data-subtext="Satra Parambh 2019"   />
                    		<figcaption>
                    			<p style="background-color: white;padding: 10px;font-weight:bold;font-size:15px;font-size: 18px;position: absolute;bottom: 15px;left: 0;right: 0;">Satra Parambh 2019</p>
                    		</figcaption>			
                    </div>
                                        <div class="col-md-3 col-sm-3 effect-ming">
                    		<img data-aos="flip-left" src="https://www.sspkm.org/documents/hostel/gallery/1602329696_photo_documents.JPG" style="height: 200px;width:98%" data-subtext="6th Sports Day"   />
                    		<figcaption>
                    			<p style="background-color: white;padding: 10px;font-weight:bold;font-size:15px;font-size: 18px;position: absolute;bottom: 15px;left: 0;right: 0;">6th Sports Day</p>
                    		</figcaption>			
                    </div>
                                        <div class="col-md-3 col-sm-3 effect-ming">
                    		<img data-aos="flip-left" src="https://www.sspkm.org/documents/hostel/gallery/1602828051_photo_documents.jpg" style="height: 200px;width:98%" data-subtext="Satra Parambh 2019"   />
                    		<figcaption>
                    			<p style="background-color: white;padding: 10px;font-weight:bold;font-size:15px;font-size: 18px;position: absolute;bottom: 15px;left: 0;right: 0;">Satra Parambh 2019</p>
                    		</figcaption>			
                    </div>
                                        <div class="col-md-3 col-sm-3 effect-ming">
                    		<img data-aos="flip-left" src="https://www.sspkm.org/documents/hostel/gallery/1602863850_photo_documents.JPG" style="height: 200px;width:98%" data-subtext="Scholarship Functions 2018"   />
                    		<figcaption>
                    			<p style="background-color: white;padding: 10px;font-weight:bold;font-size:15px;font-size: 18px;position: absolute;bottom: 15px;left: 0;right: 0;">Scholarship Functions 2018</p>
                    		</figcaption>			
                    </div>
                                    </div>
            </div>
       </div>
   </div>
</section>
<section id="documentary">
    <div class="container">
        <div class="row">
           <div class="col-md-12 text-center">
               <h2>Hostel <span class="bold900">Documentary</span></h2>
               <hr class="title-underline">
           </div>
       </div>
       <div class="row">
           <div class="col-md-6 col-sm-6" data-aos="fade-right">
               <iframe width="100%" height="315" src="https://www.youtube.com/embed/edbG2L4mJfU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
           </div>
           <div class="col-md-6 col-sm-6" data-aos="fade-left">
               <h3>
                   Our Hostels Are Living Laboratories For Sustainability And Climate Change Result.
               </h3>
               <a href="home/videos.html"><button class="theme-btn" type="button">Get More Information</button></a>
           </div>
       </div>
    </div>
</section>
<section id="counters">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <img src="assets-design/frontend/sspkm/img/counters/1.png" />
                <h2>1955</h2>
                <h3>Founded</h3>
            </div>
            <div class="col-md-3 col-sm-3">
                <img src="assets-design/frontend/sspkm/img/counters/2.png" />
                <h2><span class="num">2221</span>+</h2>
                <h3>Students</h3>
            </div>
            <div class="col-md-3 col-sm-3">
                <img src="assets-design/frontend/sspkm/img/counters/3.png" />
                <h2><span class="num">21204</span>+</h2>
                <h3>Alumni Students</h3>
            </div>
            <div class="col-md-3 col-sm-3">
                <img src="assets-design/frontend/sspkm/img/counters/4.png" />
                <h2><span class="num">20</span>+</h2>
                <h3>Near School & College</h3>
            </div>
        </div>
    </div>
</section>
<section id="query">
    <div class="container">
        <form action="#" method>
            <div class="row">
                <div class="col-md-12">
                    <h2>Contact <span class="bold900">Us</span></h2>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <input type="text" class="form-control" name="name" placeholder="Name" required />
                </div>
                <div class="form-group col-md-4">
                    <input type="email" class="form-control" name="name" placeholder="Email" required />
                </div>
                <div class="form-group col-md-4">
                    <input type="tel" class="form-control" name="name" placeholder="Phone Number" required />
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <textarea name="message" class="form-control" placeholder="Message" required></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    <button type="submit" class="theme-btn">Send Now &nbsp; <i class="fa fa-envelope"></i></button>
                </div>
            </div>
        </form>
    </div>
</section>
<section id="testimonials" style="background:linear-gradient(to bottom,#000000c7,#000000c7),url('assets-design/frontend/sspkm/img/testimonial-bg.jpg');">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center">Testimonials</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 regular slider">
                <div class="testimonial">
                    <span>&#8221;</span>
                    <p>Feel like second home, facility of living and eating is good enough, team work of trustees and rector’s is good, every festivals and so many other functions celebrates, motivational & yoga seminar also held by hostel, it’s much more than hostel.
                    </p>
                    <div><img src="assets-design/frontend/sspkm/img/female-icon.png"  /> Radhika</div>
                </div>
                <div class="testimonial">
                    <span>&#8221;</span>
                    <p>This place is only for boys who come either  from  gujarat region or whose parents lived in gujarat.<br>live like home is posses two building one has 10 floor<br>(unit 1)and other has 7 floor(unit 2).<br>Unit 2 means 7 floor building is very much good than 10 floor because unit 2 constructed on 2013.<br>It has also good rooms,lift facility,letrin,solar water heater etc.<br>Make sure to get addmission in unit 2 than unit 1</p>
                    <div><img src="assets-design/frontend/sspkm/img/male-icon.png"  /> Kaushik Vataliya</div>
                </div>
                <div class="testimonial">
                    <span>&#8221;</span>
                    <p>This is good place where the students of Patel Samaj can live at very low cost and the food which is given to the students is very good
                    </p>
                    <div><img src="assets-design/frontend/sspkm/img/male-icon.png"  /> Prince Sabhani</div>
                </div>
                <div class="testimonial">
                    <span>&#8221;</span>
                    <p>I lived here for 2 years and I loved this place its home away from home. Thats it no words to explain the feeling with the hostel.</p>
                    <div><img src="assets-design/frontend/sspkm/img/male-icon.png"  /> Kishan Sojitra</div>
                </div>
                <div class="testimonial">
                    <span>&#8221;</span>
                    <p>Best hostel facility for students whos studies in Ahmedabad far from home. With all the living and food facility and situated in best area of the city. Great facilitate for hostilities.</p>
                    <div><img src="assets-design/frontend/sspkm/img/female-icon.png"  /> Jalpa</div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="instagram-feed">
</section>


 <!-- <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script> -->
 <script src="assets-design/frontend/slider.js"></script>

  <script>
      var swiper = new Swiper(".mySwiper", {
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: {
          delay: 2500,
          disableOnInteraction: false,
        },
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
      });
</script>