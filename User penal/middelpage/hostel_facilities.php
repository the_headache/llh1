<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<body>
  
    <link rel="stylesheet" href="../assets-design/frontend/sspkm/css/hes-gallery.css">
<style>
	#banner img{
    	height:290px;
	}
	#facilities{
    	padding:40px 0;
	}
	#facilities ul{
    	list-style-type:none;
    	padding:0;
	}
	#facilities ul .facility-head img{
    	width:50px;
    	height:50px;
	}
	#facilities ul .facility-head span{
    	font-size: 20px;
    	margin-left: 20px;
	}
	#facilities ul .facility-head i{
	    opacity:0.5;
	}
	#facilities ul .facility-head span span{
	    font-weight: 900;
	}
	#facilities ul .facility-head .clearfix{
    	clear:right;
	}
	#facilities ul .facility-head button{
    	background-color:transparent;
    	border:none;
    	font-size:35px;
    	color:#385d8a;
	}
	#documentary{
    	background-color:white;
    	text-align:center;
    	padding-bottom: 30px;
	}
	#documentary h3{
    	font-weight: 900;
    	color:#002060;
    	margin-top:55px
	}
	#documentary button{
    	margin-top:20px
	}
	@media screen and (max-width:768px){
        #banner{
            height: 90px;
        }
        #banner h1{
            margin-top: 30px;
            font-size: 25px;
        }
        #banner img{
            height:90px;
        }
    }
    .hes-gallery img{
        cursor:pointer;
    }
    .hes-gallery img:hover{
        opacity:0.5;
        transition:opacity 0.3s;
    }
    .hes-gallery span{
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
    }
    .hes-gallery span{
        text-align:center;
        opacity:0;
        transition:0.8s;
        font-size:25px;
        font-weight:900;
    }
    .hes-gallery .description{
        position:absolute;
        left:10px;
        bottom:10px;
        font-weight:900;
        font-size:17px;
        opacity:0;
    }
    .hes-gallery span i{
        font-size:60px;
    }	
    .hes-gallery .item:hover > span,.hes-gallery .item:hover > .description{
        opacity:1;
    }
</style>
<section id="banner">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8 col-sm-8 col-xs-7">
				<h1>Hostel <span>Facilities</span></h1>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-5">
				<img src="assets-design/frontend/sspkm/img/hostel-facilities/banner.png" />
			</div>
		</div>
	</div>
</section>
<section id="facilities">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul>
				    
				       
					<li>
						<div class="facility-head" data-toggle="collapse" data-target="#view1">
							<button type="button" data-toggle="collapse" data-target="#view1" class="clearfix pull-right"><i class="fa fa-angle-down"></i></button>
							<img src="https://www.sspkm.org/documents/hostel/lak/1599026851_news_documents.png" />
							<span>Room</span>
						</div>
						<div id="view1" class="collapse row">
							<div class="col-md-8" style="padding-left:90px;text-align:justify;">
								<p>​​​Boys Hostel consists of two units. Unit-1 is of ten floors which consist of 160 rooms and Unit-2 is of seven floors which consists of 98 rooms.</p>

<p>Girls Hostel is of five floors. Every floor has 47 rooms plus a rector roomand a small passage for gathering. Each room has capacity of four girls. Room includes bed with drawer, book shelf, shoes rack, study table with revolving chair and cupboard for each student. Rooms have attached bathroom and washroom. And last but not the least a huge mirror which complements theperfect room a girl would need.</p>
							</div>
							<div class="col-md-4">
								<div id="myCarousel1" class="carousel slide" data-ride="carousel">
									<!-- Indicators -->
									<ol class="carousel-indicators">
									    
                                    									    
										<li data-target="#myCarousel0" data-slide-to="0" class="active"></li>
									 
				                   									    
										<li data-target="#myCarousel1" data-slide-to="1" class=""></li>
									 
				                   									    
										<li data-target="#myCarousel2" data-slide-to="2" class=""></li>
									 
				                   									    
										<li data-target="#myCarousel3" data-slide-to="3" class=""></li>
									 
				                   									    
										<li data-target="#myCarousel4" data-slide-to="4" class=""></li>
									 
				                   						
									</ol>
									<!-- Wrapper for slides -->
									<div class="carousel-inner hes-gallery">
                                        										<div class="item active">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Room</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600404820_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Room</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600404809_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Room</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600404781_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Room</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600404737_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Room</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600404760_photo_documents.jpg" alt="Los Angeles">
										</div>
												
									</div>
									<!-- Left and right controls -->
									<a class="left carousel-control" href="#myCarousel1" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
									<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#myCarousel1" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
									<span class="sr-only">Next</span>
									</a>
								</div>
							</div>
						</div>
					</li>
					   
					<li>
						<div class="facility-head" data-toggle="collapse" data-target="#view2">
							<button type="button" data-toggle="collapse" data-target="#view2" class="clearfix pull-right"><i class="fa fa-angle-down"></i></button>
							<img src="https://www.sspkm.org/documents/hostel/lak/1599027275_news_documents.png" />
							<span>Dining Hall</span>
						</div>
						<div id="view2" class="collapse row">
							<div class="col-md-8" style="padding-left:90px;text-align:justify;">
								<p>Healthy and nourishing food is also provided in the dining halls. Girls can have breakfast, lunch, and dinner while boys can choose any two options<br>
according to their convenience.<br>
Once in a week some other food apart for regular is prepared. Lastly, every alternate Sunday yummy feast is there.</p>
							</div>
							<div class="col-md-4">
								<div id="myCarousel2" class="carousel slide" data-ride="carousel">
									<!-- Indicators -->
									<ol class="carousel-indicators">
									    
                                    									    
										<li data-target="#myCarousel0" data-slide-to="0" class="active"></li>
									 
				                   									    
										<li data-target="#myCarousel1" data-slide-to="1" class=""></li>
									 
				                   									    
										<li data-target="#myCarousel2" data-slide-to="2" class=""></li>
									 
				                   						
									</ol>
									<!-- Wrapper for slides -->
									<div class="carousel-inner hes-gallery">
                                        										<div class="item active">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Dining Hall</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600926879_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Dining Hall</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600926893_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Dining Hall</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600926904_photo_documents.jpg" alt="Los Angeles">
										</div>
												
									</div>
									<!-- Left and right controls -->
									<a class="left carousel-control" href="#myCarousel2" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
									<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#myCarousel2" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
									<span class="sr-only">Next</span>
									</a>
								</div>
							</div>
						</div>
					</li>
					   
					<li>
						<div class="facility-head" data-toggle="collapse" data-target="#view3">
							<button type="button" data-toggle="collapse" data-target="#view3" class="clearfix pull-right"><i class="fa fa-angle-down"></i></button>
							<img src="https://www.sspkm.org/documents/hostel/lak/1599054834_news_documents.png" />
							<span>Library</span>
						</div>
						<div id="view3" class="collapse row">
							<div class="col-md-8" style="padding-left:90px;text-align:justify;">
								<p>A huge 24 X 7 hours open library is provided in both the hostels. It is well lighted and ventilated. It provides a peaceful environment to the students to achieve their respective goals. Some novels, magazine, and books are also available to borrow and read to acquire more than bookish knowledge</p>
							</div>
							<div class="col-md-4">
								<div id="myCarousel3" class="carousel slide" data-ride="carousel">
									<!-- Indicators -->
									<ol class="carousel-indicators">
									    
                                    									    
										<li data-target="#myCarousel0" data-slide-to="0" class="active"></li>
									 
				                   									    
										<li data-target="#myCarousel1" data-slide-to="1" class=""></li>
									 
				                   									    
										<li data-target="#myCarousel2" data-slide-to="2" class=""></li>
									 
				                   									    
										<li data-target="#myCarousel3" data-slide-to="3" class=""></li>
									 
				                   						
									</ol>
									<!-- Wrapper for slides -->
									<div class="carousel-inner hes-gallery">
                                        										<div class="item active">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Library</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600931003_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Library</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600931026_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Library</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600931043_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Library</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600931057_photo_documents.jpg" alt="Los Angeles">
										</div>
												
									</div>
									<!-- Left and right controls -->
									<a class="left carousel-control" href="#myCarousel3" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
									<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#myCarousel3" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
									<span class="sr-only">Next</span>
									</a>
								</div>
							</div>
						</div>
					</li>
					   
					<li>
						<div class="facility-head" data-toggle="collapse" data-target="#view4">
							<button type="button" data-toggle="collapse" data-target="#view4" class="clearfix pull-right"><i class="fa fa-angle-down"></i></button>
							<img src="https://www.sspkm.org/documents/hostel/lak/1599054116_news_documents.png" />
							<span>GYM</span>
						</div>
						<div id="view4" class="collapse row">
							<div class="col-md-8" style="padding-left:90px;text-align:justify;">
								<p>Boys can be in good shape and maintain their strength by the gym facility provided which has almost every needed equipment.</p>

<p>As far as girls are considered, yoga can be practiced by them in the hall way provided on every floor.</p>
							</div>
							<div class="col-md-4">
								<div id="myCarousel4" class="carousel slide" data-ride="carousel">
									<!-- Indicators -->
									<ol class="carousel-indicators">
									    
                                    									    
										<li data-target="#myCarousel0" data-slide-to="0" class="active"></li>
									 
				                   									    
										<li data-target="#myCarousel1" data-slide-to="1" class=""></li>
									 
				                   									    
										<li data-target="#myCarousel2" data-slide-to="2" class=""></li>
									 
				                   									    
										<li data-target="#myCarousel3" data-slide-to="3" class=""></li>
									 
				                   									    
										<li data-target="#myCarousel4" data-slide-to="4" class=""></li>
									 
				                   						
									</ol>
									<!-- Wrapper for slides -->
									<div class="carousel-inner hes-gallery">
                                        										<div class="item active">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">GYM</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600931069_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">GYM</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600931082_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">GYM</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600931097_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">GYM</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600931108_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">GYM</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1601891665_photo_documents.jpg" alt="Los Angeles">
										</div>
												
									</div>
									<!-- Left and right controls -->
									<a class="left carousel-control" href="#myCarousel4" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
									<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#myCarousel4" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
									<span class="sr-only">Next</span>
									</a>
								</div>
							</div>
						</div>
					</li>
					   
					<li>
						<div class="facility-head" data-toggle="collapse" data-target="#view5">
							<button type="button" data-toggle="collapse" data-target="#view5" class="clearfix pull-right"><i class="fa fa-angle-down"></i></button>
							<img src="https://www.sspkm.org/documents/hostel/lak/1599054320_news_documents.png" />
							<span>Motivation class</span>
						</div>
						<div id="view5" class="collapse row">
							<div class="col-md-8" style="padding-left:90px;text-align:justify;">
								<p>As good strength of students and reference of Trustee is available so students need not go to the institutes or classes, Tuition classes are held in the campus only. Even consultancy of competitive Examinations like GPSC, UPSC is given.</p>

<p>For Girls, various classes like Art of Living, Grooming, Spoken English, etc are held in</p>
							</div>
							<div class="col-md-4">
								<div id="myCarousel5" class="carousel slide" data-ride="carousel">
									<!-- Indicators -->
									<ol class="carousel-indicators">
									    
                                    									    
										<li data-target="#myCarousel0" data-slide-to="0" class="active"></li>
									 
				                   									    
										<li data-target="#myCarousel1" data-slide-to="1" class=""></li>
									 
				                   									    
										<li data-target="#myCarousel2" data-slide-to="2" class=""></li>
									 
				                   									    
										<li data-target="#myCarousel3" data-slide-to="3" class=""></li>
									 
				                   									    
										<li data-target="#myCarousel4" data-slide-to="4" class=""></li>
									 
				                   						
									</ol>
									<!-- Wrapper for slides -->
									<div class="carousel-inner hes-gallery">
                                        										<div class="item active">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Motivation class</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600931122_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Motivation class</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600931136_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Motivation class</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600931149_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Motivation class</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600931167_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Motivation class</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600931178_photo_documents.jpg" alt="Los Angeles">
										</div>
												
									</div>
									<!-- Left and right controls -->
									<a class="left carousel-control" href="#myCarousel5" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
									<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#myCarousel5" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
									<span class="sr-only">Next</span>
									</a>
								</div>
							</div>
						</div>
					</li>
					   
					<li>
						<div class="facility-head" data-toggle="collapse" data-target="#view6">
							<button type="button" data-toggle="collapse" data-target="#view6" class="clearfix pull-right"><i class="fa fa-angle-down"></i></button>
							<img src="https://www.sspkm.org/documents/hostel/lak/1599054376_news_documents.png" />
							<span>Fir safety</span>
						</div>
						<div id="view6" class="collapse row">
							<div class="col-md-8" style="padding-left:90px;text-align:justify;">
															</div>
							<div class="col-md-4">
								<div id="myCarousel6" class="carousel slide" data-ride="carousel">
									<!-- Indicators -->
									<ol class="carousel-indicators">
									    
                                    						
									</ol>
									<!-- Wrapper for slides -->
									<div class="carousel-inner hes-gallery">
                                        	
									</div>
									<!-- Left and right controls -->
									<a class="left carousel-control" href="#myCarousel6" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
									<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#myCarousel6" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
									<span class="sr-only">Next</span>
									</a>
								</div>
							</div>
						</div>
					</li>
					   
					<li>
						<div class="facility-head" data-toggle="collapse" data-target="#view7">
							<button type="button" data-toggle="collapse" data-target="#view7" class="clearfix pull-right"><i class="fa fa-angle-down"></i></button>
							<img src="https://www.sspkm.org/documents/hostel/lak/1599054446_news_documents.png" />
							<span>CCTV campus</span>
						</div>
						<div id="view7" class="collapse row">
							<div class="col-md-8" style="padding-left:90px;text-align:justify;">
															</div>
							<div class="col-md-4">
								<div id="myCarousel7" class="carousel slide" data-ride="carousel">
									<!-- Indicators -->
									<ol class="carousel-indicators">
									    
                                    						
									</ol>
									<!-- Wrapper for slides -->
									<div class="carousel-inner hes-gallery">
                                        	
									</div>
									<!-- Left and right controls -->
									<a class="left carousel-control" href="#myCarousel7" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
									<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#myCarousel7" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
									<span class="sr-only">Next</span>
									</a>
								</div>
							</div>
						</div>
					</li>
					   
					<li>
						<div class="facility-head" data-toggle="collapse" data-target="#view8">
							<button type="button" data-toggle="collapse" data-target="#view8" class="clearfix pull-right"><i class="fa fa-angle-down"></i></button>
							<img src="https://www.sspkm.org/documents/hostel/lak/1599054483_news_documents.png" />
							<span>Elevators (lift)</span>
						</div>
						<div id="view8" class="collapse row">
							<div class="col-md-8" style="padding-left:90px;text-align:justify;">
															</div>
							<div class="col-md-4">
								<div id="myCarousel8" class="carousel slide" data-ride="carousel">
									<!-- Indicators -->
									<ol class="carousel-indicators">
									    
                                    									    
										<li data-target="#myCarousel0" data-slide-to="0" class="active"></li>
									 
				                   						
									</ol>
									<!-- Wrapper for slides -->
									<div class="carousel-inner hes-gallery">
                                        										<div class="item active">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Elevators (lift)</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600931194_photo_documents.jpg" alt="Los Angeles">
										</div>
												
									</div>
									<!-- Left and right controls -->
									<a class="left carousel-control" href="#myCarousel8" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
									<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#myCarousel8" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
									<span class="sr-only">Next</span>
									</a>
								</div>
							</div>
						</div>
					</li>
					   
					<li>
						<div class="facility-head" data-toggle="collapse" data-target="#view9">
							<button type="button" data-toggle="collapse" data-target="#view9" class="clearfix pull-right"><i class="fa fa-angle-down"></i></button>
							<img src="https://www.sspkm.org/documents/hostel/lak/1599054518_news_documents.png" />
							<span>Hot water facility</span>
						</div>
						<div id="view9" class="collapse row">
							<div class="col-md-8" style="padding-left:90px;text-align:justify;">
															</div>
							<div class="col-md-4">
								<div id="myCarousel9" class="carousel slide" data-ride="carousel">
									<!-- Indicators -->
									<ol class="carousel-indicators">
									    
                                    									    
										<li data-target="#myCarousel0" data-slide-to="0" class="active"></li>
									 
				                   						
									</ol>
									<!-- Wrapper for slides -->
									<div class="carousel-inner hes-gallery">
                                        										<div class="item active">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Hot water facility</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600931211_photo_documents.jpg" alt="Los Angeles">
										</div>
												
									</div>
									<!-- Left and right controls -->
									<a class="left carousel-control" href="#myCarousel9" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
									<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#myCarousel9" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
									<span class="sr-only">Next</span>
									</a>
								</div>
							</div>
						</div>
					</li>
					   
					<li>
						<div class="facility-head" data-toggle="collapse" data-target="#view10">
							<button type="button" data-toggle="collapse" data-target="#view10" class="clearfix pull-right"><i class="fa fa-angle-down"></i></button>
							<img src="https://www.sspkm.org/documents/hostel/lak/1599054542_news_documents.png" />
							<span>Cold water</span>
						</div>
						<div id="view10" class="collapse row">
							<div class="col-md-8" style="padding-left:90px;text-align:justify;">
															</div>
							<div class="col-md-4">
								<div id="myCarousel10" class="carousel slide" data-ride="carousel">
									<!-- Indicators -->
									<ol class="carousel-indicators">
									    
                                    						
									</ol>
									<!-- Wrapper for slides -->
									<div class="carousel-inner hes-gallery">
                                        	
									</div>
									<!-- Left and right controls -->
									<a class="left carousel-control" href="#myCarousel10" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
									<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#myCarousel10" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
									<span class="sr-only">Next</span>
									</a>
								</div>
							</div>
						</div>
					</li>
					   
					<li>
						<div class="facility-head" data-toggle="collapse" data-target="#view11">
							<button type="button" data-toggle="collapse" data-target="#view11" class="clearfix pull-right"><i class="fa fa-angle-down"></i></button>
							<img src="https://www.sspkm.org/documents/hostel/lak/1599054566_photo_documents.png" />
							<span>Parking</span>
						</div>
						<div id="view11" class="collapse row">
							<div class="col-md-8" style="padding-left:90px;text-align:justify;">
															</div>
							<div class="col-md-4">
								<div id="myCarousel11" class="carousel slide" data-ride="carousel">
									<!-- Indicators -->
									<ol class="carousel-indicators">
									    
                                    									    
										<li data-target="#myCarousel0" data-slide-to="0" class="active"></li>
									 
				                   									    
										<li data-target="#myCarousel1" data-slide-to="1" class=""></li>
									 
				                   						
									</ol>
									<!-- Wrapper for slides -->
									<div class="carousel-inner hes-gallery">
                                        										<div class="item active">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Parking</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600931227_photo_documents.jpg" alt="Los Angeles">
										</div>
																					<div class="item ">
										    <span><i class="fa fa-search"></i><br>
                                            </span>
                                            <div class="description">Parking</div>
											<img  class="img-responsive" src="https://www.sspkm.org/documents/hostel/lak/1600931272_photo_documents.jpg" alt="Los Angeles">
										</div>
												
									</div>
									<!-- Left and right controls -->
									<a class="left carousel-control" href="#myCarousel11" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
									<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#myCarousel11" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
									<span class="sr-only">Next</span>
									</a>
								</div>
							</div>
						</div>
					</li>
									</ul>
			</div>
		</div>
	</div>
</section>
<section id="documentary">
    <div class="container">
        <div class="row">
           <div class="col-md-12 text-center">
               <h2>Hostel <span class="bold900">Documentary</span></h2>
               <hr class="title-underline">
           </div>
       </div>
       <div class="row">
           <div class="col-md-6" data-aos="fade-right">
               <iframe width="100%" height="315" src="https://www.youtube.com/embed/edbG2L4mJfU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
           </div>
           <div class="col-md-6" data-aos="fade-left">
               <h3>
                   Our Hostels Are Living Laboratories For Sustainability And Climate Change Result.
               </h3>
               <a href="#"><button class="theme-btn" type="button">Get More Information</button></a>
           </div>
       </div>
    </div>
</section>
<script src="../assets-design/frontend/sspkm/js/hes-gallery.js"></script>
<script>

    HesGallery.setOptions({
        disableScrolling: false,
        hostedStyles: false,
        animations: true,

        showImageCount: true,
        wrapAround: true
    });

</script>
<script>
        $(document).ready(function(){
        $("#view1").collapse('show');
    });
</script>    


