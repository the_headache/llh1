function isImage(file){
   return (file['type'].split('/')[0]=='image');//returns true or false
}

function readURL(input,preview) {
    
  if (input.files && input.files[0]) {
     if(isImage(input.files[0])){
        var reader = new FileReader();
        reader.onload = function(e) {
          preview.children("img").attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
     }else{
         preview.html("<h1>"+input.files[0].name+"</h1>");
     }
  }
}
function readURLSRC(input) {
  if (input.files && input.files[0]) {
     if(isImage(input.files[0])){
        var reader = new FileReader();
        reader.onload = function(e) {
        //   preview.children("img").attr('src', e.target.result);
          return e.target.files[0].name;
        }
        
        // reader.readAsDataURL(input.files[0]);
        //           return e.target.result;
     }else{
        //  preview.html(input.files[0].name);
     }
  }
}
function getFileName(myFile){
   var file = myFile.files[0];  
   var filename = file.name;
   return filename;
}
function setPreview(){
    $("#first_name_prev").html($("input[name='first_name']").val());
      $("#father_name_prev").html($("input[name='father_name']").val());
      $("#surname_prev").html($("input[name='surname']").val());
      $("#subcaste_prev").html($("select[name='subcaste']").val());
      $("#gender_prev").html($("select[name='gender']").val());
      $("#native_place_prev").html($("input[name='native_place']").val());
      $("#mobile_prev").html($("input[name='mobile']").val());
      $("#email_prev").html($("input[name='email']").val());
      $("#marital_status_prev").html($("select[name='marital_status']").val());
      $("#father_mobile_prev").html($("input[name='father_mobile']").val());
      $("#dob_prev").html($("input[name='dob']").val());
      $("#blood_group_prev").html($("select[name='blood_group']").val());
      $("#age_prev").html($("input[name='age']").val());
      $("#address_prev").html($("textarea[name='address']").val());
      $("#state_prev").html($("select[name='state'] option:selected").text());
      $("#city_prev").html($("select[name='city'] option:selected").text());
      $("#pincode_prev").html($("input[name='pincode']").val());
      $("#native_home_prev").html($("textarea[name='native_saurashtra']").val());
      $("#village_prev").html($("input[name='village']").val());
      $("#district_prev").html($("select[name='district'] option:selected").val());
      $("#taluka_prev").html($("select[name='taluka'] option:selected").val());
      $("#physically_challenged_prev").html($("select[name='physically_challenged']").val());
      
      readURL(($("input[name='photo']"))[0],$("#photo_prev"));
      
      readURL(($("input[name='id_proof']"))[0],$("#id_proof_prev"));
      
      
      
      
      
      
      
      
    
    //   $("#").html($("input[name='']").val());
}
jQuery(document).ready(function($) {
  "use strict";

  //Contact
  $(document).on("submit",'form.contactForm',function() {
      var father_mobile = $("input[name='father_mobile']").val();
      var mobile = $("input[name='mobile']").val();
      if(father_mobile==mobile){
          alert("Your mobile and Guardian's mobile should be different!");
            $('html, body').animate({
                scrollTop: $("input[name='father_mobile']").offset().top + 'px'
            }, 'fast');
          return false;
      }
      
     
      setPreview();
//validations begin:
    var f = $(this).find('.form-group'),
      ferror = false,
      emailExp = /^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i;

    f.children('input').each(function() { // run all inputs

      var i = $(this); // current input
      var rule = i.attr('data-rule');

      if (rule !== undefined) {
        var ierror = false; // error flag for current input
        var pos = rule.indexOf(':', 0);
        if (pos >= 0) {
          var exp = rule.substr(pos + 1, rule.length);
          rule = rule.substr(0, pos);
        } else {
          rule = rule.substr(pos + 1, rule.length);
        }
       
        switch (rule) {
          case 'required':
            if (i.val() === '') {
              ferror = ierror = true;
            }
            break;

          case 'minlen':
            if (i.val().length < parseInt(exp)) {
              ferror = ierror = true;
            }
            break;

          case 'email':
            if (!emailExp.test(i.val())) {
              ferror = ierror = true;
            }
            break;

          case 'checked':
            if (! i.is(':checked')) {
              ferror = ierror = true;
            }
            break;

          case 'regexp':
            exp = new RegExp(exp);
            if (!exp.test(i.val())) {
              ferror = ierror = true;
            }
            break;
          case 'photo':
            if (((i)[0].files[0].size/1024)>exp) {
              ferror = ierror = true;
            }
            break;
          case 'doc':
            if (((i)[0].files[0].size/1024/1024)>exp) {
              ferror = ierror = true;
            }
            break;
          case 'age':
            if(i.val() < parseInt(exp)){
                ferror = ierror = true;
            }
            break;
          case 'mobile':
              if (i.val().length > parseInt(exp)||i.val().length < parseInt(exp)||i.val()<5555555555||i.val()>9999999999) {
                  ferror = ierror = true;
                }
            break;
          case 'percentage':
              if (i.val() > 100 || i.val() < 32){
                  ferror = ierror = true;
              }
            break;
          case 'pincode':
              if (i.val().length!=6){
                  ferror = ierror = true;
              }
            break;
        }
        i.next('.validation').html((ierror ? (i.attr('data-msg') !== undefined ? i.attr('data-msg') : 'wrong Input') : '')).show('blind');
        $('html, body').animate({
                scrollTop: i.next('.validation').offset().top + 'px'
            }, 'fast');
      }
    });
    f.children('textarea').each(function() { // run all inputs

      var i = $(this); // current input
      var rule = i.attr('data-rule');

      if (rule !== undefined) {
        var ierror = false; // error flag for current input
        var pos = rule.indexOf(':', 0);
        if (pos >= 0) {
          var exp = rule.substr(pos + 1, rule.length);
          rule = rule.substr(0, pos);
        } else {
          rule = rule.substr(pos + 1, rule.length);
        }

        switch (rule) {
          case 'required':
            if (i.val() === '') {
              ferror = ierror = true;
            }
            break;

          case 'minlen':
            if (i.val().length < parseInt(exp)) {
              ferror = ierror = true;
            }
            break;
        }
        i.next('.validation').html((ierror ? (i.attr('data-msg') != undefined ? i.attr('data-msg') : 'wrong Input') : '')).show('blind');
        $('html, body').animate({
                scrollTop: i.next('.validation').offset().top + 'px'
            }, 'fast');
      }
    });
    if (ferror) return false;
    else{
        if($("input[name='confirm_submit']").val()=="true"){
            $(".overlay").css("display","none");
            return true;
        }else{
            $(".overlay").css("display","block");
        }
    }
    return false;
  });

});
