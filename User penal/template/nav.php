<header id="header">
       <div class="container-fluid sticky-hide">
           <div class="row top-header">
               <div class="col-md-6 col-sm-6 hidden-xs">
                   <a href="mailto:+918866032888"><i class="fa fa-phone"></i> +91 8866032888</a>
                   &nbsp;
                   &nbsp;
                   &nbsp;
                   <a href="mailto:sphostel@gmail.com" class="hidden-xs"><i class="fa fa-envelope"></i> llhhostel@gmail.com</a>
               </div>
               <div class="col-md-6 col-sm-6 col-xs-12 social text-right">
                   <a href="https://www.facebook.com/pages/category/Hostel/Sp_hostel_offical-105637484591837/" target="_blank"><img src="assets-design/frontend/sspkm/img/social/fb.png" /></a>
                   <a href="https://instagram.com/sp_hostel_official?igshid=14nc7gvhmhnmy" target="_blank"><img src="assets-design/frontend/sspkm/img/social/insta.png" /></a>
                   <a href="https://www.youtube.com/channel/UCHeXQ_tHCSG3tGVt6Mvt09A" target="_blank"><img src="assets-design/frontend/sspkm/img/social/youtube.png" /></a>
               </div>
           </div>
            <div class="row">
                <!--<div class="col-md-12"><hr></div>-->
            </div>
       </div>
       <div class="container-fluid">
           <div class="row logo">
               <div class="col-md-3 col-sm-3 col-xs-12">
                   <div class="row">
                       <div class="col-md-5 col-sm-5 col-xs-3">
                           <a href="index.php"><img src="assets-design/frontend/sspkm/img/logo2.png" /></a>
                       </div>
                       <div class="col-md-7 col-sm-7 col-xs-5">
                           <!--<a href="https://www.sspkm.org/">-->
                           <!--    <h3 class="first-title">Shree Saurashtra</h3>-->
                           <!--    <h3>Patel Kelavani</h3>-->
                           <!--    <h3>Mandal</h3> -->
                           <!--</a>-->
                       </div>
                       <div class="hidden-lg hidden-md hidden-sm col-xs-4">
                           <ul class="mobile-ul">
                               <li class="mobile-li">
                                   <a class="menu-btn" href="javascript:void(0);"><i class="fa fa-bars"></i> MENU</a>
                                   <ul class="mobile-nav">
                                       <li><a href="index.php" class="active">Home</a></li>
                                       <li><a href="index.php?page=about">About Us</a></li>
                                       <li class="submenu"><a href="javascript:void(0);">Student Life</a>
                                           <ul>
                                               <li><a href="index.php?page=hostel_facilities">Hostel Facilities</a></li>
                                               <li class="subsubmenu"><a href="javascript:void(0);">Events and Functions</a>
                                                    <ul>
                                                        <li><a href="index.php?page=photos">Photos</a></li>
                                                        <li><a href="home/videos.html">Videos</a></li>
                                                    </ul>
                                               </li>
                                           </ul>
                                       </li>
                                       <li class="submenu"><a href="javascript:void(0);">Admission</a>
                                       
                                       </li>
                                  
                                       <!-- <li><a href="home/news.html">News</a></li> -->
                                       <li><a href="index.php?page=contactus">Contact Us</a></li>
                                   </ul>
                               </li>
                           </ul>
                       </div>
                   </div>
               </div>
               <div class="col-md-9 col-sm-9 hidden-xs text-center">
                   <ul class="pc-nav">
                       <li><a href="index.php" class=" active">Home</a></li>
                       <li><a href="index.php?page=about" class="">About Us</a></li>
                       <li class="submenu"><a href="#" class="">Student Life</a>
                           <ul>
                               <li><a href="index.php?page=hostel_facilities">Hostel Facilities</a></li>
                               <li class="subsubmenu"><a href="#">Events and Functions</a>
                                    <ul>
                                        <li><a href="index.php?page=photos">Photos</a></li>
                                        <li><a href="index.php?page=videos">Videos</a></li>
                                    </ul>
                               </li>
                           </ul>
                       </li>

                       <li class="submenu"><a href="javascript:void(0);">Admission</a>
                                           <ul>
                                               <li><a href="index.php?page=admission_form">ADMISSION FORM</a></li>
                                               
                                               <li><a href="index.php?page=pay_fees">PAY FEES ONLINE</a></li>
                                               
                                               <li><a href="index.php?page=updatedocuments">UPDATE DOCUMENTS</a></li>
                                           </ul>
                                       </li>

                       <!-- <li class="submenu"><a href="#" class="">Admission</a>
                                 
                               
                                     <ul>
                                      <?php 
                                        $i=1;
                                          $select="SELECT * FROM `category`";
                                          $qry=mysqli_query($con,$select);
                                          while($row=mysqli_fetch_assoc($qry))
                                          {
                                          

                                        ?>
                                               <li><a href="index.php?page=admission_form"><?php echo $row['c_name'];?></a></li>

                                               
                                          <?php $i++; } ?> 
                                      </ul>
                       </li> -->
                       <!-- <li class="submenu"><a href="#" class="">Publication</a>
                            <ul>
                               <li><a href="home/patel_suvas.html">Patel Suvas</a></li>
                               <li><a href="home/foreign_report.html">Foreign Report</a></li>
                           </ul>
                       </li> -->
                       <!-- <li><a href="index.php?page=news" class="">News</a></li> -->
                       <li><a href="index.php?page=contactus" class="">Contact Us</a></li>
                   </ul>
               </div>
           </div>
       </div>
   </header>
   