<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <div class="text-center">
                    <a href="index.php"><img width="150px" src="assets-design/frontend/sspkm/img/logo2.png" /></a>
                </div>
                <div class="text-center">
                    <a href="https://goo.gl/maps/iX61XifsefqojTYi9" class="view-on-map" target="_blank">View On Map</a>
                </div>
                <ul class="address">
                    <li>
                       Live LIke home ,<br>
                        Near new IIM rd, Vastrapur,<br> 
                        Ahmedabad, Gujarat-380009
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="footer-column-content">
                    <h3>Contact Info</h3>
                    <ul>
                        <li><a href="tel:+9107926307099"><i class="fa fa-phone"></i> +91 079 26307099</a></li>
                        <li><a href="tel:+9107926307099"><i class="fa fa-phone"></i> +91 63520 96322</a></li>
                        <li><a href="mailto:sphostel@gmail.com"><i class="fa fa-envelope"></i> llhhostel@gmail.com</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="footer-column-content">
                    <h3>Links</h3>
                    <ul>
                        <li><a href="index.php?page=admission_form">Admission Form</a></li>
                       <!--  <li><a href="home/downloads.html">Downloads</a></li>
                        <li><a href="home/promotion.html">Student Promotion</a></li>
                        <li><a href="home/patel_suvas.html">Patel Suvas</a></li>
                        <li><a href="home/news.html">News</a></li>
                        --> <li><a href="index.php?page=updatedocuments">Update Documents</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="footer-column-content">
                   <h3>Recent Posts</h3>
                    facebook content here
                </div>
            </div>
        </div>
    </div>
    <!--<div class="container-fluid lower-footer">-->
    <!--    <div class="row">-->
    <!--        <div class="col-md-6 col-sm-6">Shree Saurashtra Patel Kelavani Mandal</div>-->
    <!--        <div class="col-md-6 col-sm-6 social text-right">-->
    <!--           <a href="https://www.facebook.com/pages/category/Hostel/Sp_hostel_offical-105637484591837/" target="_blank"><img src="https://www.sspkm.org/assets-design/frontend/sspkm/img/social/fb.png" /></a>-->
    <!--           <a href="https://instagram.com/sp_hostel_official?igshid=14nc7gvhmhnmy" target="_blank"><img src="https://www.sspkm.org/assets-design/frontend/sspkm/img/social/insta.png" /></a>-->
    <!--           <a href="https://www.youtube.com/channel/UCHeXQ_tHCSG3tGVt6Mvt09A" target="_blank"><img src="https://www.sspkm.org/assets-design/frontend/sspkm/img/social/youtube.png" /></a>-->
    <!--       </div>-->
    <!--    </div>-->
    <!--</div>-->
    <div class="container-fluid designed-by">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <a href="home/privacy_policy.html">Privacy Policy</a> |
                    <a href="home/privacy_policy.html">Terms of Use</a>
                </div>
                <div class="col-md-6 col-sm-6">
                    <span class="credits">
                    Website Developed by: <a href="https://www.ragingdevelopers.com/" target="_blank"> <span class="heart"></span> LJCCA Student</a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</footer>