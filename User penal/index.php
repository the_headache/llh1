<?php

  include "database/dbconnect.php";

  ?>

<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title>LLH | Home</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../assets-design/frontend/sspkm/img/logo2.png">
    
    <link rel="stylesheet" href="../maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets-design/frontend/sspkm/css/styleec74.css?v=0.07" />
    <link rel="stylesheet" type="text/css" href="assets-design/frontend/sspkm/css/style-responsivea139.html?v=0.01" />
    
    <link rel="stylesheet" href="../cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css">
    <!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">-->
  <link
      rel="stylesheet"
      href="assets-design/frontend/slider.css"
    />

    <link rel="stylesheet" type="text/css" href="../unpkg.com/aos%402.3.0/dist/aos.css" />
    
    <script src="assets-design/frontend/sspkm/js/jquery-3.4.1.js"></script>
    <script src="../stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

    <style>
        
        .footer-column-content{
            width:max-content;
            margin:auto;
        }
        @media screen and (max-width:1252px){
            header .logo img{
                margin-left:0;
            }
        }
        @media screen and (max-width:992px){
            header .logo h3{
                font-size:18px;
            }
            .sticky .logo h3 {
                font-size: 16px;
            }
            header .logo img{
                width:80px;
            }
            .sticky .logo img{
                width:75px;
            }
            header .pc-nav{
                margin:6px 0;
                margin-top:15px;
            }
            .sticky .pc-nav{
                margin-top:10px;
            }
            header .pc-nav li a{
                padding: 1px 0;
                margin: 0 6px;
                font-size: 15px;
            }
        }
        @media screen and (max-width:768px){
            .footer-column-content{
                margin-left:30px;
            }
            .social{
                text-align:center;
            }
        }
        .scroll-top {
            width: 50px;
            height: 50px;
            position: fixed;
            bottom: 25px;
            right: 20px;
            display: none;
            opacity: 0.6;
        }
        .scroll-top i {
            display: inline-block;
            color: #FFFFFF;
        }
    </style>
</head>
<body>
   <!---------------------------------- header Start ---------------------------- -->
   <?php

        include "template/nav.php";
    
   ?>
   <!---------------------------------- header End----------------------------- -->


    <link rel="stylesheet" href="assets-design/frontend/sspkm/css/hes-gallery.css">
<link rel="stylesheet" type="text/css" href="assets-design/frontend/sspkm/css/slick/slick.css">
<link rel="stylesheet" type="text/css" href="assets-design/frontend/sspkm/css/slick/slick-theme.css">
<link rel="stylesheet" type="text/css" href="assets-design/frontend/sspkm/css/index307f.css?v=0.04" />
<link rel="stylesheet" type="text/css" href="assets-design/frontend/sspkm/css/set2a139.css?v=0.01" />
<style>

#header{
    position:fixed;
    width:100%;
}
@media screen and (max-width:768px){
      #about-us .news-col .date{
          margin:auto;
      }
      #header{
        position:relative;
        width:100%;
      }
      #header.sticky{
          position:fixed;
      }
    #slider{
        margin-top:0;
    }
    #about-us .news-col .date .month{
        font-size:18px;
    }
    #about-us .news-col .date{
        width:112px;
    }
}
#facilities a{
    color:black;
}
#facilities a:hover{
    color:black;
}
</style>

<!------------------------- main page start -------------------- -->

<?php
  
  @$page= $_REQUEST['page'];

  if ($page=="" && basename($_SERVER['PHP_SELF'])=='index.php')
  {
    $page = 'home';
  }
  if ($page!="" && file_exists("middelpage/".$page.".php"))
  {
    include ("middelpage/".$page.".php");
  }
  else
  {
    include "middelpage/error.php";
  }

  ?>
  
  
<!-- ------------------main page end ---------------------- -->


 <!-- <div id="myModal" class="modal fade" style="z-index: 10000;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center">Notice</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                 </div>
        </div>
    </div> -->
<script src="assets-design/frontend/sspkm/js/hes-gallery.js"></script>

<script>

    HesGallery.setOptions({
        disableScrolling: false,
        hostedStyles: false,
        animations: true,

        showImageCount: true,
        wrapAround: true
    });

</script>
<script src="assets-design/frontend/sspkm/js/slick/slick.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
   $(function() {
      $(".regular").slick({
        dots: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay:true,
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
      });
    });
</script>
<script src="../cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.js"></script>
<script src="../cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js"></script>
<script>
    $(document).ready(function(){
        $('.num').counterUp({
          delay: 10,
          time: 1000
        });
        $('.num').addClass('animated fadeInDownBig');
    });
    
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#myModal").modal('show');
        $("figcaption").click(function(){
            $(this).parent().children("img").click();
        });
    });
</script>   


<!---------------------------------- footer start----------------------------- -->
<?php
  
  include "template/footer.php";

  ?>

<!---------------------------------- footer end----------------------------- -->


<button class="btn btn-primary scroll-top" data-scroll="up" type="button">
<i class="fa fa-chevron-up"></i>
</button>
<script src="assets-design/frontend/sspkm/js/main9093.js?v=0.02"></script>
<script src="../unpkg.com/aos%402.3.0/dist/aos.js"></script>
<script>
    AOS.init({
  duration: 1200,
})
</script>
<script>
    window.onscroll = function() {myFunction()};
    
    var header = document.getElementById("header");
    var sticky = header.offsetTop;
    
    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
        $(".sticky-hide").fadeOut();
      } else {
        header.classList.remove("sticky");
        $(".sticky-hide").fadeIn();
      }
    }
</script>
</body>

</html>