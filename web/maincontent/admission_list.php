<?php

    include "dbconnection.php";


  ?>
<!DOCTYPE html>

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<title> Admin Panel  Home :: LLM</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Visitors Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap-css -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
<!-- //bootstrap-css -->
<!-- Custom CSS -->
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <link href="css/style-responsive.css" rel="stylesheet"/>
<!-- font CSS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!-- font-awesome icons -->
    <link rel="stylesheet" href="css/font.css" type="text/css"/>
    <link href="css/font-awesome.css" rel="stylesheet"> 
    <link rel="stylesheet" href="css/morris.css" type="text/css"/>
<!-- calendar -->
    <link rel="stylesheet" href="css/monthly.css">
<!-- //calendar -->
<!-- //font-awesome icons -->
<script src="js/jquery2.0.3.min.js"></script>
<script src="js/raphael-min.js"></script>
<script src="js/morris.js"></script>
</head>
<body>
  
  <table class="table table-striped">
       
        <thead>
          <tr>
             <tr>
          <th> <a href="index.php?page=admission_form.php" class="btn btn-primary">Add New Category</a>
   </th>
        </tr>
            <th data-breakpoints="xs">Sr No</th>
            <th>Student's Full Name</th>
            <th>Caste</th>
            <th>Gender</th>
            <th>M_no</th>
            <th>email</th>
            <th>Father_M_no</th>
            <th>Date Of Birth</th>
            <th>Age</th>
            <th>Address</th>
            <th>City</th>
            <th>Pincode</th>
            <th>Village</th>
            <th>Dist</th>
            <th>S_photo</th>
            <th>leaveing_cer</th>
            <th>Id_Proof</th>
            <th>Fees_receipt</th>
            <th>Course_name</th>
            <th>Semester</th>
            <th>Marksheets_photo</th>
            <th>edu_cat</th>
            <th>edu_subcat</th>
            <th>Passing_Year</th>
            <th>Board</th>
            <th>Percentage</th>                               
            <th>Action</th>

          </tr>
        </thead>
        <tbody>
          <?php
            $i=1;
            $select="SELECT * FROM `admission`";
            $qry=mysqli_query($con,$select);
            while($row=mysqli_fetch_assoc($qry))
           {
              
            ?>
          <tr data-expanded="true">
            <td><?php echo $i; ?></td>
            <td><?php echo $row['name'];?></td>
            <td><?php echo $row['caste'];?></td>
            <td><?php echo $row['gender'];?></td>
            <td><?php echo $row['M_no'];?></td>
            <td><?php echo $row['email'];?></td>
            <td><?php echo $row['FM_no'];?></td>
            <td><?php echo $row['DOB'];?></td>
            <td><?php echo $row['age'];?></td>
            <td><?php echo $row['Address'];?></td>
            <td><?php echo $row['city'];?></td>
            <td><?php echo $row['pin'];?></td>
            <td><?php echo $row['village'];?></td>
            <td><?php echo $row['dist'];?></td>
            <td><img height="60" width="60"  src="<?php echo "../User penal/upload/".$row['s_photo'];?>"></td>
            <td><img height="60" width="60"  src="<?php echo "../User penal/upload/".$row['leaving_cer'];?>"></td>
            <td><img height="60" width="60"  src="<?php echo "../User penal/upload/".$row['id_Proof'];?>"></td>  
            <td><img height="60" width="60"  src="<?php echo "../User penal/upload/".$row['fees_receipt'];?>"></td>
            <td><?php echo $row['course_name'];?></td>
            <td><?php echo $row['semester'];?></td>
           <td><img height="60" width="60"  src="<?php echo "../User penal/upload/".$row['marksheets_file'];?>"></td>
            <td><?php echo $row['edu_cat'];?></td>
            <td><?php echo $row['edu_subcat'];?></td>
            <td><?php echo $row['passing_year'];?></td>
            <td><?php echo $row['board'];?></td>
            <td><?php echo $row['percentage'];?></td>
            <td>
              <a class="btn btn-success" href="index.php?page=admission_form&action=update&c_id=<?php echo $row['id'];?>">Update</a>
              ||
              <a class="btn btn-danger" href="index.php?page=admission_action&action=delete&c_id=<?php echo $row['id'];?>">Delete</a>

            </td>
            
          </tr>
         
        </tbody>
        <?php $i++; } ?>
      </table>
</body>

 